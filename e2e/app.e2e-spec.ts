import { TestdashPage } from './app.po';

describe('testdash App', function() {
  let page: TestdashPage;

  beforeEach(() => {
    page = new TestdashPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
